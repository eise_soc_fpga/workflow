# Copyright 2021 Raphaël Bresson
# buildroot initial build
${BUILD_DIR}/buildroot:
	@echo "----------------------------------------------------------------"
	@echo "---           PREPARE BUILDROOT BUILD DIRECTORY              ---"
	@echo "----------------------------------------------------------------"
	@echo "### INFO: Cloning Xilinx Buildroot repository in directory ${PWD}/$@"
	@cd ${BUILD_DIR} && git clone https://github.com/buildroot/buildroot.git -b 2022.11
	@echo "### INFO: Configuring Buildroot"
	@make O=${BUILD_DIR}/buildroot-output BR2_EXTERNAL=${PWD}/os -C ${BUILD_DIR}/buildroot ${BR2_DEFCONFIG}

.PHONY: buildroot-force-defconfig
buildroot-force-defconfig:
	@echo "### INFO: Configuring Buildroot"
	@make O=${BUILD_DIR}/buildroot-output BR2_EXTERNAL=${PWD}/os -C ${BUILD_DIR}/buildroot ${BR2_DEFCONFIG}

# clean buildroot
.PHONY: buildroot-clean
buildroot-clean:
	@echo "### INFO: Cleaning Buildroot outputs"
	@rm -rf ${BUILD_DIR}/buildroot ${BUILD_DIR}/buildroot-output

# update buildroot
${BUILD_DIR}/buildroot-output/images/boot/BOOT.BIN: os/board/${BR2_BOARD}/dts/system-top.dts os/board/${BR2_BOARD}/fsbl.elf | ${BUILD_DIR}/buildroot
	@echo "----------------------------------------------------------------"
	@echo "---              BUILDROOT DISTRIBUTION BUILD                ---"
	@echo "----------------------------------------------------------------"
	@make O=${BUILD_DIR}/buildroot-output BR2_EXTERNAL=${PWD}/os -C ${BUILD_DIR}/buildroot

.PHONY: buildroot-force-update
buildroot-force-update:
	@echo "### INFO: Building/Updating Buildroot GNU/Linux distribution"
	@make O=${BUILD_DIR}/buildroot-output BR2_EXTERNAL=${PWD}/os -C ${BUILD_DIR}/buildroot

.PHONY: buildroot-update
buildroot-update: ${BUILD_DIR}/buildroot-output/images/boot/BOOT.BIN

.PHONY: buildroot-cmd
buildroot-cmd:
	@echo "### INFO: Building Buildroot target: ${CMD}"
	@if [[ "${CMD}" == *[!\ ]* ]]; then \
		make O=${BUILD_DIR}/buildroot-output BR2_EXTERNAL=${PWD}/os ${CMD} -C ${BUILD_DIR}/buildroot; \
	else \
		echo "### ERROR: Mandatory argument \"CMD\" not specified for target $@"; \
		echo "### USAGE: make buildroot-cmd CMD=<your command>"; \
		echo "### EXEMPLE: make buildroot-cmd CMD=menuconfig"; \
	fi

.PHONY: buildroot-cpio-build
buildroot-cpio-build: ${BUILD_DIR}/buildroot-output/images/boot/BOOT.BIN
	@echo "### INFO: Building Buildroot CPIO RootFS"
	@make O=${BUILD_DIR}/buildroot-output BR2_EXTERNAL=${PWD}/os -C ${BUILD_DIR}/buildroot ${BR2_CPIO_DEFCONFIG}
	@make O=${BUILD_DIR}/buildroot-output BR2_EXTERNAL=${PWD}/os -C ${BUILD_DIR}/buildroot
	@make O=${BUILD_DIR}/buildroot-output BR2_EXTERNAL=${PWD}/os -C ${BUILD_DIR}/buildroot ${BR2_DEFCONFIG}

