# Copyright 2021 Raphaël Bresson

SYNTH_V_FILES        = $(shell find ${PWD}/rtl/synth/ -name "*.v")
SYNTH_SV_FILES       = $(shell find ${PWD}/rtl/synth/ -name "*.sv"   | grep -v pkg | grep -v _top) $(shell find ${PWD}/rtl/synth/ -name "*.sv"   | grep -v pkg | grep _top)
SYNTH_VHD_FILES      = $(shell find ${PWD}/rtl/synth/ -name "*.vhd"  | grep -v pkg | grep -v _top) $(shell find ${PWD}/rtl/synth/ -name "*.vhd"  | grep -v pkg | grep _top)
SYNTH_VHDL_FILES     = $(shell find ${PWD}/rtl/synth/ -name "*.vhdl" | grep -v pkg | grep -v _top) $(shell find ${PWD}/rtl/synth/ -name "*.vhdl" | grep -v pkg | grep _top)
SYNTH_PKG_SV_FILES   = $(shell find ${PWD}/rtl/synth/ -name "*.sv"   | grep pkg)
SYNTH_PKG_VHD_FILES  = $(shell find ${PWD}/rtl/synth/ -name "*.vhd"  | grep pkg)
SYNTH_PKG_VHDL_FILES = $(shell find ${PWD}/rtl/synth/ -name "*.vhdl" | grep pkg)
SYNTH_XCI_FILES      = $(shell find ${PWD}/rtl/synth/ -name "*.xci" )
SYNTH_BD_FILES       = $(shell find ${PWD}/rtl/synth/ -name "*.bd"  )

CONSTR_XDC_PRE_SYNTH   = $(shell find ${PWD}/rtl/pre_synth/  -name "*.xdc")
CONSTR_TCL_PRE_SYNTH   = $(shell find ${PWD}/rtl/pre_synth/  -name "*.tcl")

CONSTR_XDC_POST_SYNTH  = $(shell find ${PWD}/rtl/post_synth/ -name "*.xdc")
CONSTR_TCL_POST_SYNTH  = $(shell find ${PWD}/rtl/post_synth/ -name "*.tcl")

CONSTR_XDC_PROBES         = $(shell find ${PWD}/rtl/probes/ -name "*.xdc")
CONSTR_TCL_PROBES         = $(shell find ${PWD}/rtl/probes/ -name "*.tcl")

CONSTR_XDC_PRE_OPT        = $(shell find ${PWD}/rtl/pre_opt/  -name "*.xdc")
CONSTR_TCL_PRE_OPT        = $(shell find ${PWD}/rtl/pre_opt/  -name "*.tcl")

CONSTR_XDC_POST_OPT       = $(shell find ${PWD}/rtl/post_opt/ -name "*.xdc")
CONSTR_TCL_POST_OPT       = $(shell find ${PWD}/rtl/post_opt/ -name "*.tcl")

CONSTR_XDC_PRE_PLACEMENT  = $(shell find ${PWD}/rtl/pre_placement/  -name "*.xdc")
CONSTR_TCL_PRE_PLACEMENT  = $(shell find ${PWD}/rtl/pre_placement/  -name "*.tcl")

CONSTR_XDC_POST_PLACEMENT = $(shell find ${PWD}/rtl/post_placement/ -name "*.xdc")
CONSTR_TCL_POST_PLACEMENT = $(shell find ${PWD}/rtl/post_placement/ -name "*.tcl")

CONSTR_XDC_PRE_ROUTE  = $(shell find ${PWD}/rtl/post_route/ -name "*.xdc")
CONSTR_TCL_PRE_ROUTE  = $(shell find ${PWD}/rtl/post_route/ -name "*.tcl")

CONSTR_XDC_POST_ROUTE = $(shell find ${PWD}/rtl/post_route/ -name "*.xdc")
CONSTR_TCL_POST_ROUTE = $(shell find ${PWD}/rtl/post_route/ -name "*.tcl")

CONSTR_XDC_PRE_BITSTREAM = $(shell find ${PWD}/rtl/pre_bitstream/ -name "*.xdc")
CONSTR_TCL_PRE_BITSTREAM = $(shell find ${PWD}/rtl/pre_bitstream/ -name "*.tcl")

CONSTR_PRE_SYNTH      = ${CONSTR_TCL_PRE_SYNTH}      ${CONSTR_XDC_PRE_SYNTH}
CONSTR_POST_SYNTH     = ${CONSTR_TCL_POST_SYNTH}     ${CONSTR_XDC_POST_SYNTH}
CONSTR_PROBES         = ${CONSTR_TCL_PROBES}         ${CONSTR_XDC_PROBES}
CONSTR_PRE_OPT        = ${CONSTR_TCL_PRE_OPT}        ${CONSTR_XDC_PRE_OPT}
CONSTR_POST_OPT       = ${CONSTR_TCL_POST_OPT}       ${CONSTR_XDC_POST_OPT}
CONSTR_PRE_PLACEMENT  = ${CONSTR_TCL_PRE_PLACEMENT}  ${CONSTR_XDC_PRE_PLACEMENT}
CONSTR_POST_PLACEMENT = ${CONSTR_TCL_POST_PLACEMENT} ${CONSTR_XDC_POST_PLACEMENT}
CONSTR_PRE_ROUTE      = ${CONSTR_TCL_PRE_ROUTE}      ${CONSTR_XDC_PRE_ROUTE}
CONSTR_POST_ROUTE     = ${CONSTR_TCL_POST_ROUTE}     ${CONSTR_XDC_POST_ROUTE}
CONSTR_PRE_BITSTREAM  = ${CONSTR_TCL_PRE_BITSTREAM}  ${CONSTR_XDC_PRE_BITSTREAM}


SIM_V_FILES    = $(shell find ${PWD}/rtl/sim/ -name "*.v"   )
SIM_SV_FILES   = $(shell find ${PWD}/rtl/sim/ -name "*.sv"  )
SIM_VHD_FILES  = $(shell find ${PWD}/rtl/sim/ -name "*.vhd" )
SIM_VHDL_FILES = $(shell find ${PWD}/rtl/sim/ -name "*.vhdl")

GEN_PROTOINST_FILES            = $(shell find ${PWD}/build/xsim/build/ -name ".protoinst")
IPSHARED_DIR                   = ${PWD}/build/xsim/build/ipshared
GENERATED_IPSHARED_DIRNAMES    = $(shell ls ${IPSHARED_DIR})
GENERATED_IPSHARED_DIRS        = $(addprefix ${IPSHARED_DIR}/, ${GENERATED_IPSHARED_DIRNAMES})
GENERATED_IPSHARED_HDLDIRS     = $(addsuffix /hdl , ${GENERATED_IPSHARED_DIRS})
GENERATED_IPSHARED_VERILOGDIRS = $(addsuffix /hdl/verilog , ${GENERATED_IPSHARED_DIRS})
GENERATED_IPSHARED_INCLUDE     = $(addprefix --include , ${GENERATED_IPSHARED_HDLDIRS} ${GENERATED_IPSHARED_VERILOGDIRS})
PROTOINST_DECLARE              = $(addprefix --protoinst , ${GEN_PROTOINST_FILES})

