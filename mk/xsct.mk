# Copyright 2021 Raphaël Bresson
XSCT_SCRIPTS_DIR = ${PWD}/script/xsct
XSCT_FOLDER  = ${BUILD_DIR}/xsct
XSCT_WS      = ${XSCT_FOLDER}/workspace
BAREMETAL_PLATFORM = pfm_baremetal

${XSCT_FOLDER}:
	@echo "-----------------------------------------"
	@echo "---  XSCT BUILD DIRECTORIES CREATION  ---"
	@echo "-----------------------------------------"
	@mkdir -p $@

${XSCT_WS}: | ${XSCT_FOLDER}
	@mkdir -p $@

${XSCT_FOLDER}/device-tree-xlnx: | ${XSCT_WS}
	@echo "### INFO: Cloning Xilinx device-tree repository in directory ${PWD}/$@"
	@cd ${XSCT_FOLDER} && git clone https://github.com/Xilinx/device-tree-xlnx -b xilinx_v2022.2

${XSCT_FOLDER}/dts/system-top.dts: ${BUILD_DIR}/vivado/system.xsa | ${XSCT_FOLDER}/device-tree-xlnx
	@echo "-------------------------------------------"
	@echo "---   BOOT FILES GENERATION FOR LINUX   ---"
	@echo "-------------------------------------------"
	@echo "### INFO: Running xsct hsi"
	@xsct ${XSCT_SCRIPTS_DIR}/hsi.tcl "${BUILD_DIR}" "${SOC_FAMILY}" "${XSCT_BOARD}"

os/board/${BR2_BOARD}/dts/system-top.dts: ${XSCT_FOLDER}/dts/system-top.dts
	@echo "### INFO: Copying and patching generated device-tree files from ${PWD}/${XSCT_FOLDER}/dts/ to ${PWD}/os/board/${BR2_BOARD}/dts/"
	@mkdir -p os/board/${BR2_BOARD}/dts
	@cp $< $@
	@touch os/board/${BR2_BOARD}/dts/pl.dtsi
	@find ${XSCT_FOLDER}/dts/ -name "*.dtsi" -exec cp {} os/board/${BR2_BOARD}/dts \;
	@find ${XSCT_FOLDER}/dts/ -name "*.h"    -exec cp {} os/board/${BR2_BOARD}/dts \;
	@find ${XSCT_FOLDER}/dts/ -name "*.dts"  -exec cp {} os/board/${BR2_BOARD}/dts \;
	@find ${XSCT_FOLDER}/dts/ -name "*.bit"  -exec cp {} os/board/${BR2_BOARD}/dts \;
	@sed -i '/\/ {/i #include "system-user.dtsi"' os/board/${BR2_BOARD}/dts/system-top.dts
	@echo "### INFO: Copying fsbl.elf from ${XSCT_FOLDER}/fsbl to ${PWD}/os/board/${BR2_BOARD}"
	@cp ${XSCT_FOLDER}/fsbl/executable.elf os/board/${BR2_BOARD}/fsbl.elf
	@if [ "${SOC_FAMILY}" == "zynqmp" ]; then \
	  echo "### INFO: Patching ZynqMPSoc device-tree for buildroot"; \
	  for f in `find os/board/${BR2_BOARD}/dts/ -name "*.dts*"`; do \
	    sed -i 's/include\/dt\-bindings\/dma\///g'     $${f}; \
	    sed -i 's/include\/dt\-bindings\/gpio\///g'    $${f}; \
	    sed -i 's/include\/dt\-bindings\/power\///g'   $${f}; \
	    sed -i 's/include\/dt\-bindings\/reset\///g'   $${f}; \
	    sed -i 's/include\/dt\-bindings\/clock\///g'   $${f}; \
	    sed -i 's/include\/dt\-bindings\/phy\///g'     $${f}; \
	    sed -i 's/include\/dt\-bindings\/pinctrl\///g' $${f}; \
	    sed -i 's/include\/dt\-bindings\/input\///g'   $${f}; \
	  done; \
	  echo "### INFO: Copying and patching pm_cfg_obj from ${XSCT_FOLDER}/fsbl/zynqmp_fsbl_bsp/psu_cortexa53_0/libsrc/xilpm_v4_1/src/ to ${PWD}/os/board/${BR2_BOARD}"; \
	  cp ${XSCT_FOLDER}/fsbl/zynqmp_fsbl_bsp/psu_cortexa53_0/libsrc/xilpm_v4_1/src/pm_cfg_obj.c os/board/${BR2_BOARD}/pm_cfg_obj.c; \
	  cp ${XSCT_FOLDER}/fsbl/zynqmp_fsbl_bsp/psu_cortexa53_0/libsrc/xilpm_v4_1/src/pm_cfg_obj.h os/board/${BR2_BOARD}/pm_cfg_obj.h; \
	  sed -i '/^#define PM_CONFIG_OBJECT_TYPE_BASE/d' os/board/${BR2_BOARD}/pm_cfg_obj.c; \
	  sed -i 's/PM_CONFIG_OBJECT_TYPE_BASE/0x1U/g' os/board/${BR2_BOARD}/pm_cfg_obj.c; \
	  echo "### INFO: Copying pmufw.elf from ${XSCT_FOLDER}/pmufw to ${PWD}/os/board/${BR2_BOARD}"; \
	  cp ${XSCT_FOLDER}/pmufw/executable.elf os/board/${BR2_BOARD}/pmufw.elf; \
	  echo "### INFO: Copying psu_init_gpl.c from ${BUILD_DIR}/vivado to ${PWD}/os/board/${BR2_BOARD}"; \
	  cp ${BUILD_DIR}/vivado/psu_init.c os/board/${BR2_BOARD}/psu_init_gpl.c; \
	  cp ${BUILD_DIR}/vivado/psu_init.h os/board/${BR2_BOARD}/psu_init_gpl.h; \
	  sed -i 's+psu_init.h+psu_init_gpl.h+g' os/board/${BR2_BOARD}/psu_init_gpl.c; \
	else \
	  echo "### INFO: Copying ps7_init_gpl.c from ${BUILD_DIR}/vivado to ${PWD}/os/board/${BR2_BOARD}"; \
	  cp ${BUILD_DIR}/vivado/ps7_init_gpl.c os/board/${BR2_BOARD}/ps7_init_gpl.c; \
	  cp ${BUILD_DIR}/vivado/ps7_init_gpl.h os/board/${BR2_BOARD}/ps7_init_gpl.h; \
	fi

.PHONY: sys-update
sys-update: os/board/${BR2_BOARD}/dts/system-top.dts

### ------------------------------------------------------------------------------------------

BARE_METAL_C = $(shell find baremetal/ -name "*.c")
BARE_METAL_H = $(shell find baremetal/ -name "*.h")

${XSCT_WS}/${BAREMETAL_PLATFORM}/platform.spr: ${BUILD_DIR}/vivado/system.xsa ${XSCT_FOLDER}/dts/system-top.dts
	@echo "-------------------------------------------"
	@echo "---    BAREMETAL PLATFORM GENERATION    ---"
	@echo "-------------------------------------------"
	@echo "### INFO: Generating baremetal platform"
	@xsct ${XSCT_SCRIPTS_DIR}/baremetal_platform.tcl "${BUILD_DIR}" "${SOC_FAMILY}"
	@echo "### INFO: Baremetal platform successfully generated"

.PHONY: xsct-clean-workspace
xsct-clean-workspace:
	@echo "### INFO: Cleaning baremetal workspace"
	@rm -rf ${XSCT_WS} ${XSCT_FOLDER}/*.done

${XSCT_FOLDER}/bm-verify-${BM_PROJECT}.done: | ${XSCT_WS}
	@if [[ "${BM_PROJECT}" == *[!\ ]* ]]; then \
		if [[ "$(shell find baremetal/ -name ${BM_PROJECT})" == *[!\ ]* ]]; then \
			echo "done" > ${XSCT_FOLDER}/bm-verify-${BM_PROJECT}.done \
			echo "### INFO: Found baremetal project: baremetal/${BM_PROJECT}"; \
		else \
			echo "### ERROR: Project ${BM_PROJECT} not found in \"baremetal/\" directory"; \
			exit -1; \
		fi \
	else \
		echo "### ERROR: BM_PROJECT argument containing the name of baremetal project is mandatory for all xsct-baremetal-* targets"; \
		echo "### USAGE: make xsct-baremetal-<target> BM_PROJECT=<name of your project>"; \
		echo "### EXEMPLE: BUILD: make xsct-baremetal-build BM_PROJECT=helloworld"; \
		echo "### EXEMPLE: RUN: make xsct-baremetal-run BM_PROJECT=helloworld"; \
		echo "### EXEMPLE: DEBUG: make xsct-baremetal-debug BM_PROJECT=helloworld"; \
		echo "### INFO: Any baremetal project must be placed in \"baremetal\" directory"; \
		exit -1; \
	fi

${XSCT_WS}/${BM_PROJECT}/Debug/${BM_PROJECT}.elf: ${XSCT_WS}/${BAREMETAL_PLATFORM}/platform.spr ${XSCT_FOLDER}/bm-verify-${BM_PROJECT}.done ${BARE_METAL_C} ${BARE_METAL_H}
	@echo "----------------------------------------------------------------"
	@echo "---           BAREMETAL APPLICATION COMPILATION              ---"
	@echo "----------------------------------------------------------------"
	@if [ ! -d ${XSCT_WS}/${BM_PROJECT} ]; then \
		echo "### INFO: Generating application project ${BM_PROJECT}"; \
	  xsct -nodisp ${XSCT_SCRIPTS_DIR}/baremetal_application.tcl ${BUILD_DIR} ${BM_PROJECT} ${PWD}/baremetal/${BM_PROJECT}/src 1; \
	else \
		echo "### INFO: Updating application project ${BM_PROJECT}"; \
	  xsct -nodisp ${XSCT_SCRIPTS_DIR}/baremetal_application.tcl ${BUILD_DIR} ${BM_PROJECT} ${PWD}/baremetal/${BM_PROJECT}/src 0; \
	fi
	@echo "### INFO: Built executable: ${XSCT_WS}/${BM_PROJECT}/Debug/${BM_PROJECT}.elf"

.PHONY: baremetal-build
baremetal-build: ${XSCT_WS}/${BM_PROJECT}/Debug/${BM_PROJECT}.elf

.PHONY: baremetal-run
baremetal-run: baremetal-build
	@echo "----------------------------------------------------------------"
	@echo "---                BAREMETAL APPLICATION RUN                 ---"
	@echo "----------------------------------------------------------------"
	@echo "### INFO: Running baremetal project ${BM_PROJECT}"
	@xsct -nodisp ${XSCT_SCRIPTS_DIR}/boot.tcl "${BUILD_DIR}" "${SOC_FAMILY}" "baremetal" "${XSCT_WS}/${BM_PROJECT}/Debug/${BM_PROJECT}.elf" "0" "${NVERBOSE}"
	@echo "### INFO: baremetal project ${BM_PROJECT} run successfully finished"

.PHONY: baremetal-debug
baremetal-debug: baremetal-build
	@echo "----------------------------------------------------------------"
	@echo "---               BAREMETAL APPLICATION DEBUG                ---"
	@echo "----------------------------------------------------------------"
	@echo "### INFO: Debugging baremetal project ${BM_PROJECT}"
	@xsct -nodisp -interactive ${XSCT_SCRIPTS_DIR}/boot.tcl "${BUILD_DIR}" "${SOC_FAMILY}" "baremetal" "${XSCT_WS}/${BM_PROJECT}/Debug/${BM_PROJECT}.elf" "1" "${NVERBOSE}"
	@echo "### INFO: baremetal project ${BM_PROJECT} debug session successfully finished"

.PHONY: xsct-clean
xsct-clean: xsct-clean-workspace
	@echo "### INFO: Cleaning xsct folder"
	@rm -rf build/xsct

.PHONY: vitis
vitis: ${XSCT_WS}/${BAREMETAL_PLATFORM}/platform.spr
	@vitis -workspace ${XSCT_WS} > /dev/null 2>&1
	@echo "Would you like to save the modified files? [y, N]"
	@read rc; \
	if [[ "$${rc}" == @(y|Y) ]]; then \
		echo "### INFO: Copying source files from ${PWD}/${XSCT_WS}/ to ${PWD}/baremetal"; \
		for d in `ls -d ${XSCT_WS}/*/ | grep -v hw | grep -v bsp | grep -v TempFiles`; do \
      proj_dir=$$(basename $${d}); \
			mkdir -p baremetal/$${proj_dir}; \
			cp -r ${XSCT_WS}/$${proj_dir}/src baremetal/$${proj_dir}/; \
		done \
	else \
		echo "### INFO: Modified files from ${PWD}/${XSCT_WS}/ not saved"; \
		echo "### INFO: Files will be automatically restored on the next baremetal build"; \
	fi

.PHONY: jtag-boot-buildroot
jtag-boot-buildroot: buildroot-cpio-build
	@echo "----------------------------------------------------------------"
	@echo "---      BUILDROOT GNU/LINUX DISTRIBUTION BOOT FROM JTAG     ---"
	@echo "----------------------------------------------------------------"
	@xsct ${XSCT_SCRIPTS_DIR}/boot.tcl "${BUILD_DIR}" "${SOC_FAMILY}" "linux" "${NVERBOSE}"
	@echo "### INFO: Linux boot successfully done"

