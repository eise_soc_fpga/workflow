# TOOLS USED
USE_VIVADO    = YES
USE_XSIM      = YES
USE_GHDL      = YES
USE_BUILDROOT = YES

# FPGA IMPLEMENTATION DIRECTIVES
SYNTHESIS_DIRECTIVE  = Default
OPT_DESIGN_DIRECTIVE = Default
PLACEMENT_DIRECTIVE  = Default
ROUTE_DIRECTIVE      = Default

# CONSTRAINTS
PRE_SYNTH_CONSTRAINTS  =
POST_SYNTH_CONSTRAINTS =

PROBES_CONSTRAINTS += ${PROJECT_DIR}/xilinx_kria/constrs/pre-opt-probes.xdc

PRE_OPT_CONSTRAINTS += ${PROJECT_DIR}/xilinx_kria/constrs/pre-opt-pinout.xdc
POST_OPT_CONSTRAINTS += ${PROJECT_DIR}/xilinx_kria/constrs/post-opt-power_opt.tcl

PRE_PLACEMENT_CONSTRAINTS   =
POST_PLACEMENT_CONSTRAINTS += ${PROJECT_DIR}/xilinx_kria/constrs/post-placement-phys_opt.tcl

PRE_ROUTE_CONSTRAINTS   =
POST_ROUTE_CONSTRAINTS += ${PROJECT_DIR}/xilinx_kria/constrs/post-route-phys_opt.tcl

PRE_BITSTREAM_CONSTRAINTS =

TOP      = spectrum_analyzer_top
SIM_TOP ?= tb_spectrum_analyzer_top

#include rtl/sigma_delta_core/sources.mk
#include rtl/yuv420_to_grayscale/sources.mk
#include rtl/locked_dma/sources.mk
#include rtl/simple_adder/sources.mk

SYNTH_SRC += ${PROJECT_DIR}/xilinx_kria/synth/design_1.bd
SYNTH_SRC += ${PROJECT_DIR}/xilinx_kria/synth/spectrum_analyzer_top.vhd
SIM_SRC   += ${PROJECT_DIR}/xilinx_kria/sim/tb_design_1.sv
SIM_SRC   += ${PROJECT_DIR}/xilinx_kria/sim/tb_spectrum_analyzer_top.sv
