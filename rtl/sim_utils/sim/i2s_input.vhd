library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity i2s_input_model is
  generic( P_BCLK_LRCK_RATIO : integer := 64;
           P_SAMPLE_SIZE     : integer := 24);
  port( data : out std_logic;
        bclk : in  std_logic;
        lrck : in  std_logic);
end entity;

architecture behav of i2s_input_model is
  constant C_SAMPLE_LENGTH : integer := P_BCLK_LRCK_RATIO / 2;
	constant C_DUMMY_CYCLES : integer := C_SAMPLE_LENGTH - P_BCLK_LRCK_RATIO;
  signal r_data : std_logic_vector(C_SAMPLE_LENGTH - 1 downto 0);
  signal r_count : integer;
begin
  model_data: process begin
    wait until rising_edge(bclk);
    data <= r_data(0);
    if(r_count < (C_SAMPLE_LENGTH -1)) then
      r_data <= r_data(C_SAMPLE_LENGTH - 2 downto 0) & '0';
      r_count <= r_count + 1;
    else
      r_data(C_SAMPLE_LENGTH - 1 downto C_DUMMY_CYCLES) <= new_random_sample;
      r_count <= 0;
    end if;
  end process;
  data <= r_data(C_SAMPLE_LENGTH);
end architecture;
