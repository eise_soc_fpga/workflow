#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

void generate_random_samples(int64_t *samples, size_t nb_samples, int64_t min_val, int64_t max_val) {
  for(int i = 0 ; i < nb_samples ; i++) {
    samples[i] = (rand() % (max_val - min_val)) + min_val;
  }
}

void bits_str(char *bits, int64_t sample, size_t nbits) {
  for(size_t i = 0 ; i < nbits ; i++) {
    bits[i] = (sample >> (nbits-i-1)) & 0x1;
  }
}

int main(int argc, char **argv) {
  char filename[1024];
  char bits[64];
  if(argc < 4) {
    printf("Usage: argv[0] work_directory data_width samples_number");
    return -1;
  }
  const char *prefix  = argv[1];
  size_t data_width = atoul(argv[2]);
  if(data_width > 64) {
    printf("Not supported\n");
    return -1;
  }
  size_t nb_samples  = atoul(argv[3]);
  int64_t min_val = -pow(2,(data_width-1));
  int64_t max_val = pow(2,(data_width-1))-1;
  int64_t in0, in1, out;
  sprintf(filename, "%s_in0.dec.txt", prefix);
  FILE *fdec_in0 = fopen(filename, "w");
  sprintf(filename, "%s_in1.dec.txt", prefix);
  FILE *fdec_in1 = fopen(filename, "w");
  sprintf(filename, "%s_in0.hex.txt", prefix);
  FILE *fhex_in0 = fopen(filename, "w");
  sprintf(filename, "%s_in1.hex.txt", prefix);
  FILE *fhex_in1 = fopen(filename, "w");
  sprintf(filename, "%s_in0.bin.txt", prefix);
  FILE *fbin_in0 = fopen(filename, "w");
  sprintf(filename, "%s_in1.bin.txt", prefix);
  FILE *fbin_in1 = fopen(filename, "w");
  sprintf(filename, "%s_gold.dec.txt", prefix);
  FILE *fdec_gold = fopen(filename, "w");
  sprintf(filename, "%s_gold.hex.txt", prefix);
  FILE *fhex_gold = fopen(filename, "w");
  sprintf(filename, "%s_gold.bin.txt", prefix);
  FILE *fbin_gold = fopen(filename, "w");
  for(size_t i = 0 ; i < nb_samples; i++) {
    in0 = (rand() % (max_val - min_val + 1)) + min_val;
    fprintf(fdec_in0, "%ld\n", in0);
    fprintf(fhex_in0, "0x%lx\n", in0);
    fprintf(fbin_in0, "%s", bits);
    in1 = (rand() % (max_val - min_val + 1)) + min_val;
    fprintf(fdec_in1, "%ld\n", in1);
    fprintf(fhex_in1, "0x%lx\n", in1);
    fprintf(fbin_in1, "%s", bits);
    out = in0 + in1;
  }
  fclose(fdec_in0);
  fclose(fhex_in0);
  fclose(fbin_in0);
  fclose(fdec_in1);
  fclose(fhex_in1);
  fclose(fbin_in1);
  fclose(fdec_gold);
  fclose(fhex_gold);
  fclose(fbin_gold);
  return 0;
}
