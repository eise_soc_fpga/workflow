#include <bits/c++config.h>
#include <cstdint>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <cmath>
#include <bitset>

int main(int argc, char **argv) {
  if(argc < 4) {
    std::cout << "Usage: " << argv[0] << " work_directory data_width nb_samples"
              << std::endl;
    return -1;
  }
  std::string work(argv[1]);
  std::size_t data_width = std::stoul(argv[2]);
  if(data_width > 64) {
    std::cout << "Error: data_width > 64 not supported. Got " << data_width
              << std::endl;
    return -1;
  }
  std::size_t nb_samples = std::stoul(argv[3]);
  std::int64_t min_val = -std::pow(2, (data_width - 1));
  std::int64_t max_val = std::pow(2, (data_width - 1)) - 1;
  std::ofstream in_0_hex(work + "/in_0.hex.txt");
  std::ofstream in_0_dec(work + "/in_0.dec.txt");
  std::ofstream in_0_bin(work + "/in_0.bin.txt");
  std::ofstream in_1_hex(work + "/in_1.hex.txt");
  std::ofstream in_1_dec(work + "/in_1.dec.txt");
  std::ofstream in_1_bin(work + "/in_1.bin.txt");
  std::ofstream gold_hex(work + "/gold.hex.txt");
  std::ofstream gold_dec(work + "/gold.dec.txt");
  std::ofstream gold_bin(work + "/gold.bin.txt");
  for(std::size_t i = 0 ; i < nb_samples ; i++) {
    int64_t in_0 = (rand() % (max_val - min_val + 1)) + min_val;
    int64_t in_1 = (rand() % (max_val - min_val + 1)) + min_val;
    int64_t gold = in_0 + in_1;
    in_0_hex << std::hex << in_0 << "\n";
    in_1_hex << std::hex << in_1 << "\n";
    gold_hex << std::hex << gold << "\n";
    in_0_dec << std::dec << in_0 << "\n";
    in_1_dec << std::dec << in_1 << "\n";
    gold_dec << std::dec << gold << "\n";
    for(std::size_t j = 0 ; j < data_width ; j++) {
      in_0_bin << ((in_0 >> (data_width - 1 - i)) & 0x1);
      in_1_bin << ((in_1 >> (data_width - 1 - i)) & 0x1);
      gold_bin << ((gold >> (data_width - 1 - i)) & 0x1);
   }
   in_0_bin << "\n";
   in_1_bin << "\n";
   gold_bin << "\n";
  }
}
