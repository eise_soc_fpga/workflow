if {$argc > 0} {
  set build_dir [lindex $argv 0]
} else {
  puts "### ERROR: Argument not specified (build_dir)"
  exit -1
}

if {$argc > 1} {
  set app_name [lindex $argv 1]
} else {
  puts "### ERROR: Argument not specified (app_name)"
  exit -1
}

if {$argc > 2} {
  set app_path [lindex $argv 2]
} else {
  puts "### ERROR: Argument not specified (app_path)"
  exit -1
}

if { $argc > 3 } {
  set baremetal_new [lindex $argv 3]
} else {
  puts "### WARNING: Argument not specified (baremetal_new): setting to one"
  set baremetal_new 1
}

#set c_sources [glob -directory $app_path/*.c -nocomplain]
#if { [llength $c_sources] == 0 } {
#  puts "### ERROR: No c file found in directory $app_path"
#  exit -1
#}

setws $build_dir/xsct/workspace
platform active pfm_baremetal
if { $baremetal_new == 1 } {
  puts "### INFO: Creating application project $app_name"
  app create -name $app_name -template {Empty Application} -platform pfm_baremetal
}
importsources -name $app_name -path $app_path
puts "### INFO: Compiling application $app_name"
app build -name $app_name

unset app_name
unset app_path
unset baremetal_new
unset build_dir
